# docker-compose.yml para levantar flectrahq_argentina
**NOTA IMPORTANTE: En la carpeta donde se ubica docker-compose.yml crear la carpeta addons.**

### Instrucciones

1. Clonar el repositorio

```console
git clone https://gitlab.com/alitux/flectra-argentina-docker-compose
```
2. Entrar en la carpeta del repo y crear la carpeta addons
```console
mkdir addons
```

3. Levantar los servicios

```console
docker-compose up -d
```
